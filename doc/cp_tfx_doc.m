% We start creating an example 50 x 40 x 30 tensor with rank R = 5 and add 10% noise.

R = 5;
info = create_problem('Size', [50 40 30], 'Num_Factors', R, 'Noise', 0.10);
T = info.Data;
M_true = info.Soln;

% Set the display options to 3 so we can see the iterations.
options.display=3;

% Here is an example call to the cp_tfx method.
[factors, T_approx, output] = cp_tfx(T, R, options);

% It's important to check the output of the optimization method. In particular, it's worthwhile to check the stop messages. 
stop_msg = output.stop_msg

% We can "score" the similarity of the model computed by CP and compare that with the truth. The score function on ktensor's
% gives a score in [0,1] with 1 indicating a perfect match. Because we have noise, we do not expect the fit to be perfect.
scr = score(factors, M_true)

% It can be also interesting to review the set of options used for this particular CP computation. Tuning the parameters to 
% optimize the solution is an important part of the process. Except for the parameter display which we set manually, the 
% remaining ones are set to their defaults. 
output.options

% To finish we plot the evolution of the errors, step sizes, error improvements and gradient norms.
figure(1);
semilogy(output.errors{1})
hold on;
semilogy(output.step_sizes{1})
semilogy(output.improv{1})
semilogy(output.gradients{1})
legend('Relative errors', 'Step sizes', 'Improvements', 'Gradients')
xlabel('Iteration')
hold off;
