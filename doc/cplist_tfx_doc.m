% Create folder 'inputs'.
mkdir inputs

% Create and save several tensors and their ranks. We consider zero noise to make the
% example simpler, otherwise other folder with the true tensors should be created.
options.display = 0;
ranks = {};
i = 1;

for n=50:60    
    % Ranks
    R = n - 45;
    ranks{i} = num2str(R);
    % Tensors
    info = create_problem('Size', n*ones(1, 4), 'Num_Factors', R, 'Noise', 0.0);
    X = info.Data;
    T = double(X);
    M_true = info.Soln;
    tensor_path = "inputs/tensor_" + num2str(i) + ".mat";
    save(tensor_path, 'T', '-v7.3', '-nocompression');
    % Options
    options_path = "inputs/options_" + num2str(i) + ".mat";
    save(options_path, 'options')
    i = i+1;
end

% Save arrays of ranks.
fileID = fopen('inputs/ranks.txt', 'w');
fprintf(fileID, '%s\n', ranks{:});
fclose(fileID);

% Now we need to create two text files, one with the paths to the tensors and one
% with the paths to the options. These files must be saved at the current workspace.
% We show how to create it in Matlab but you can use your preferred way.
tensor_txt = {};
options_txt = {};
i = 1;

for n=50:60
    tensor_txt{i} = "inputs/tensor_" + num2str(i) + ".mat";
    options_txt{i} = "inputs/options_" + num2str(i) + ".mat";
    i = i+1;
end

% Each variable xxx_paths points to the files (or values in the case of ranks) to be used
% by each call of the CP decomposition. The sequence is taken line by line of each file.
tensor_paths = 'tensor_paths.txt';
fileID = fopen(tensor_paths, 'w');
fprintf(fileID, '%s\n', tensor_txt{:});
fclose(fileID);

ranks_path = 'inputs/ranks.txt';

options_paths = 'options_paths.txt';
fileID = fopen(options_paths, 'w');
fprintf(fileID, '%s\n', options_txt{:});
fclose(fileID);

[errors, timings] = cplist_tfx(tensor_paths, ranks_path, options_paths);

% Plot results, except the first one because of the compilation time. 
figure(1);
semilogy([51:60], errors(2:end), 'bs')
hold on;
semilogy([51:60], errors(2:end), 'b')
title('Relative errors')
xlabel('n')
ylabel('Error')
hold off;

figure(2);
plot([51:60], timings(2:end), 'rs')
hold on;
plot([51:60], timings(2:end), 'r')
title('Elapsed time')
xlabel('n')
ylabel('Seconds')
hold off;
