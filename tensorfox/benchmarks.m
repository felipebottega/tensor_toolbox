function results = benchmarks(R, num_modes, min_dim, delta, max_dim)
% This function computes the CP decomposition of tensors of shape n x n x ... x n (num_modes times),
% where n = min_dim, min_dim + delta, min_dim + 2*delta, ..., max_dim. 
% The computation are perfomed with the algorithms ALS and OPT from TEnsor Toolbox, and the algorithm
% of Tensor Fox. At the end of the computations plots will be displayed.


% Create folder 'inputs'.
mkdir inputs

% Create and save several tensors and their ranks. We consider zero noise to make the
% example simpler, otherwise other folder with the true tensors should be created.
% All solvers will be tested with their default parameters. Only the best p=70% of the
% results are considered. We do this as an attempt to avoid considering bad solutions 
% when computing the average errors. 
options.display = 0;
OPTIONS.printEvery = 1000; 
values = [min_dim:delta:max_dim];
m = length(values);
ranks = {};
num_trials = 20;
p = 0.70;
i = 1;
j = 1;

% Arrays for errors and timings.
temp_cp_opt_errors = zeros(1, num_trials);
cp_opt_errors = zeros(1, m);
cp_opt_timings = zeros(1, m);

temp_cp_als_errors = zeros(1, num_trials);
cp_als_errors = zeros(1, m);
cp_als_timings = zeros(1, m);

cp_tfx_save_timings = zeros(1, m);
temp_cp_tfx_errors = zeros(1, num_trials);
cp_tfx_errors = zeros(1, m);
cp_tfx_timings = zeros(1, m);

for n=values    
    for tr=1:num_trials
        % Create tensor
        ranks{j} = num2str(R);
        info = create_problem('Size', n*ones(1, num_modes), 'Num_Factors', R, 'Noise', 0.0);
        X = info.Data;
        M_true = info.Soln;

        % Save tensor
        tic;
        T = double(X);
        tensor_path = "inputs/tensor_" + num2str(j) + ".mat";
        save(tensor_path, 'T', '-v7.3', '-nocompression');
        cp_tfx_save_timings(i) = cp_tfx_save_timings(i) + toc;
        disp(' ')
        disp('===========================================================')
        disp(['Tensor shape = ', num2str(size(T)), '    Rank = ', num2str(R)])
        disp(' ')

        % Save Tensor Fox options
        options_path = "inputs/options_" + num2str(j) + ".mat";
        save(options_path, 'options')
        j = j+1;

        % cp opt
        tic;
        [M, M0, output] = cp_opt(X, R, 'opt_options', OPTIONS);
        T_approx = double(M);
        rel_error = frob(T - T_approx)/frob(T)
        temp_cp_opt_errors(tr) = rel_error;
        cp_opt_timings(i) = cp_opt_timings(i) + toc;      

        % cp als  
        tic;
        [M, M0, output] = cp_als(X, R, 'printitn', 1000);
        T_approx = double(M);
        rel_error = frob(T - T_approx)/frob(T)
        temp_cp_als_errors(tr) = rel_error;
        cp_als_timings(i) = cp_als_timings(i) + toc; 
    end

    % Update cp tfx saving timings
    cp_tfx_save_timings(i) = cp_tfx_save_timings(i)/num_trials;

    % Update cp opt results
    temp_cp_opt_errors = sort(temp_cp_opt_errors);
    cp_opt_errors(i) = mean(temp_cp_opt_errors(1:ceil(p*num_trials)));
    cp_opt_timings(i) = cp_opt_timings(i)/num_trials;

    % Update cp als results
    temp_cp_als_errors = sort(temp_cp_als_errors);
    cp_als_errors(i) = mean(temp_cp_als_errors(1:ceil(p*num_trials)));
    cp_als_timings(i) = cp_als_timings(i)/num_trials;
    i = i+1;
end

% Save arrays of ranks.
fileID = fopen('inputs/ranks.txt', 'w');
fprintf(fileID, '%s\n', ranks{:});
fclose(fileID);

% Now we need to create two text files, one with the paths to the tensors and one
% with the paths to the options. These files must be saved at the current workspace.
% We show how to create it in Matlab but you can use your preferred way.
tensor_txt = {};
options_txt = {};

for j=1:length(values)*num_trials
    tensor_txt{j} = "inputs/tensor_" + num2str(j) + ".mat";
    options_txt{j} = "inputs/options_" + num2str(j) + ".mat";
end

% Each variable xxx_paths points to the files to be used by each call of the CP decomposition. 
% The sequence is taken line by line of each file.
tensor_paths = 'tensor_paths.txt';
fileID = fopen(tensor_paths, 'w');
fprintf(fileID, '%s\n', tensor_txt{:});
fclose(fileID);

% Ranks pointer to the ranks list
ranks_path = 'ranks_path.txt';
f = fopen(ranks_path, 'wt') ;
fprintf(f, '%s', 'inputs/ranks.txt');
fclose(f);

options_paths = 'options_paths.txt';
fileID = fopen(options_paths, 'w');
fprintf(fileID, '%s\n', options_txt{:});
fclose(fileID);

[errors, timings] = cplist_tfx(tensor_paths, ranks_path, options_paths);

% Calculates the averages of cp tfx.
i = 1;
j = 1;
for n=values
    for tr=1:num_trials
        temp_cp_tfx_errors(tr) = errors(j);
        cp_tfx_timings(i) = cp_tfx_timings(i) + timings(j);
        j = j+1;
    end
    temp_cp_tfx_errors = sort(temp_cp_tfx_errors);
    cp_tfx_errors(i) = mean(temp_cp_tfx_errors(1:ceil(p*num_trials)));
    cp_tfx_timings(i) = cp_tfx_timings(i)/num_trials;
    i = i+1;
end

% Since this Matlab script doesn't have access to the loading timings we use the estimate 
% time(save + load) ~ 2*time(save).
cp_tfx_save_timings = 2*cp_tfx_save_timings;

% Save results in a struct called 'results'. The first CP computation is discarded because it has 
% the compilation time of cp tfx affecting the timing.
results.values = values(2:end);
results.cp_opt_errors = cp_opt_errors(2:end);
results.cp_opt_timings = cp_opt_timings(2:end);
results.cp_als_errors = cp_als_errors(2:end); 
results.cp_als_timings = cp_als_timings(2:end); 
results.cp_tfx_errors = cp_tfx_errors(2:end);
results.cp_tfx_timings = cp_tfx_timings(2:end);
results.cp_tfx_save_timings = cp_tfx_save_timings(2:end);

% Plot results. 
figure(1);
semilogy(results.values, results.cp_opt_errors, 'bs')
hold on;
semilogy(results.values, results.cp_als_errors, 'rs')
semilogy(results.values, results.cp_tfx_errors, 'ks')
semilogy(results.values, results.cp_opt_errors, 'b')
semilogy(results.values, results.cp_als_errors, 'r')
semilogy(results.values, results.cp_tfx_errors, 'k')
title('Relative errors')
legend('cp opt', 'cp als', 'cp tfx')
xlabel('n')
ylabel('Error')
hold off;

figure(2);
plot(results.values, results.cp_opt_timings, 'bs')
hold on;
plot(results.values, results.cp_als_timings, 'rs')
plot(results.values, results.cp_tfx_timings, 'ks')
plot(results.values, results.cp_tfx_timings + results.cp_tfx_save_timings, 'gs')
plot(results.values, results.cp_opt_timings, 'b')
plot(results.values, results.cp_als_timings, 'r')
plot(results.values, results.cp_tfx_timings, 'k')
plot(results.values, results.cp_tfx_timings + results.cp_tfx_save_timings, 'g')
title('Elapsed time')
legend('cp opt', 'cp als', 'cp tfx', 'cp tfx + save and load')
xlabel('n')
ylabel('Seconds')
hold off;
